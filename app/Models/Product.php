<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;


class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    protected $appends = [
        'total_quantity',
        'image_path',
    ];

    public function units()
    {
        return $this->belongsToMany(Unit::class,'product_unit')->withPivot('amount');
    }

    public function getTotalQuantityAttribute()
    {
        $productUnits = $this->units;
        $total_quantity = 0 ;
        foreach($productUnits as $unit){
            $temp_quantity = $unit->modifier * $unit->pivot->amount;

            $total_quantity += $temp_quantity;
        }
        return $total_quantity;
    }

    public function getImagePathAttribute()
    {
        return URL::to('/') . '/storage/' . $this->Image->path ;
    }

    public function Image()
    {
        return $this->morphOne(Image::class, 'o');
    }

}
