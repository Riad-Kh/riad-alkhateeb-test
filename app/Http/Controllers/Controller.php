<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\URL;

use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    const Error_Message = "Some Thing is Wrong";
    const Successfully = "Successfully";

    public function returnError($msg, $ex = '', $statusCode = 400)
        {
            if($ex != '')
                $statusCode = 500;

            return response()->json([
                'isOk' => false,
                'message' => $msg,
                'exception' => $ex
                ], $statusCode);
        }

    public function returnSuccess($msg , $ex = '' )
        {
            return response()->json([
                'isOk' => true,
                'message' => $msg,
                'exception' => $ex
            ],200);
        }

    public  function uploadImage($image , $fileName)
        {
            $rand = rand(0 , 9999999);
            $imageName =  time() . $rand .'.'. $image->extension();
            $imageToSave = $fileName . '/' . time(). $rand . '.'. $image->extension();
            $image->move(public_path('storage'. DIRECTORY_SEPARATOR .$fileName), $imageName);
            return $imageToSave;
        }

    public  function getImage($image)
        {
            if(isset($image) && !empty($image)) {
                $image = str_replace('\\\\', '/', $image);
                $image = str_replace('\\', '/', $image);
                return URL::to('/') . '/storage/' . $image ;
            }
            else return null;
        }


}
