<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if($request->o_type == 'user')
            {
                $user = User::findOrfail($request->o_id);
                Image::query()->create([
                    'o_id' => $user->id,
                    'o_type' => User::class,
                    'path' => self::uploadImage($request->image,Image::image_directory),
                    'description' => $request->description,
                ]);
            }
            elseif($request->o_type == 'product'){
                $product = Product::findOrfail($request->o_id);
                Image::query()->create([
                    'o_id' => $product->id,
                    'o_type' => Product::class,
                    'path' => self::uploadImage($request->image,Image::image_directory),
                    'description' => $request->description,
                ]);
            }
        }catch(\Exception $ex){
            return   self::returnError(self::Error_Message, $ex->getMessage(),$ex->getCode());
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        //
    }
}
